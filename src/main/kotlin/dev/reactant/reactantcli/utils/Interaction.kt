package dev.reactant.reactantcli.utils

import java.util.*
import kotlin.system.exitProcess

class Interaction(val scanner: Scanner) {
    fun askYN(question: String, default: Boolean): Boolean {
        val defaultValue = "(${if (default) "Y" else "y"}/${if (!default) "N" else "n"})"
        return when (default) {
            true -> ask("$question $defaultValue") { true }.toLowerCase() != "n"
            false -> ask("$question $defaultValue") { true }.toLowerCase() == "y"
        }
    }

    fun ask(question: String): String = ask(question) { true }
    fun ask(question: String, validator: (String) -> Boolean): String {
        while (true) {
            System.out.print(question)
            try {
                scanner.nextLine().let {
                    if (validator(it)) return it
                }
            } catch (e: NoSuchElementException) {
                exitProcess(1)
            }
        }
    }

    fun ask(question: String, default: String) = ask(question).let { if (it.isBlank()) default else it }

    fun ask(question: String, validator: (String) -> Boolean, default: String) = ask(question, validator)
        .let { if (it.isBlank()) default else it }

    fun askNotBlank(question: String) = ask(question) { it.isNotBlank() }


}
