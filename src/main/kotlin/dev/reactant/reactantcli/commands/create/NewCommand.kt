package dev.reactant.reactantcli.commands.create

import dev.reactant.reactantcli.commands.create.NewProjectConfigKeys.*
import dev.reactant.reactantcli.utils.Interaction
import dev.reactant.reactantcli.utils.VersionUtils
import freemarker.template.Configuration
import freemarker.template.Template
import me.tongfei.progressbar.ProgressBar
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.BatchingProgressMonitor
import org.slf4j.LoggerFactory
import picocli.CommandLine
import java.io.File
import java.io.FileWriter
import java.util.*
import kotlin.collections.LinkedHashMap
import kotlin.system.exitProcess


@CommandLine.Command(name = "new", description = ["Create a new reactant plugin project"])
class NewCommand : Runnable {
    val logger = LoggerFactory.getLogger(this.javaClass)
    @CommandLine.Parameters(arity = "1", paramLabel = "PROJECT_NAME")
    var projectName: String = ""
    val interaction = Interaction(Scanner(System.`in`))

    @CommandLine.Option(
        names = ["-b", "--branch"],
        paramLabel = "BRANCH",
        defaultValue = "master",
        description = ["specify the version of template"]
    )
    var branch: String = "master"

    val projectConfig = LinkedHashMap<NewProjectConfigKeys, Any>();

    private lateinit var projectDir: File;

    private lateinit var progressBar: ProgressBar

    override fun run() {
        projectDir = File(projectName);

        checkDirNotExist()

        setProjectInfo()

        progressBar = ProgressBar("Create Project", 3);

        try {

            projectDir.mkdir()
            cloneRepository()
            replacePlaceHolder()
            gitInit()
            progressBar.extraMessage = "Done"
        } catch (e: Exception) {
            progressBar.close()
            e.printStackTrace()
            System.err.println("Failed to create project.")
            exitProcess(1)
        }
        progressBar.close()

        if (projectConfig[BINTRAY] as Boolean) System.out.println("To enable bintray publishing, build with environment variables: BINTRAY_USER, BINTRAY_KEY")
        if (projectConfig[AUTODEPLOY] as Boolean) System.out.println("To auto deploy to development server, build with environment variable: PLUGIN_DEPLOY_PATH")
        System.out.println("====================================")
        System.out.println("Congratulations, project created successfully.")
        System.out.println("This is an experimental framework. If you found anything weird, report to us :D")
        System.out.println("To get started, check our document here. https://reactant.dev")
        System.out.println("Finally, please join our discord and share your experience with us! https://discord.gg/9NufxVr")
    }

    private fun gitInit() {
        progressBar.extraMessage = "Git init"
        Git.init().setDirectory(projectDir).call()
        progressBar.step()
    }


    private fun setProjectInfo() {
        while (true) {
            projectConfig.clear()
            projectConfig[PROJECT_NAME] = projectName;

            val groupValidator: (String) -> Boolean = {
                val valid = (it.matches(Regex("^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*\$")) && it.isNotBlank())
                if (!valid) System.out.println("Invalid group name")
                valid
            }
            projectConfig[GROUP] = interaction.ask("Group (example: com.yourname):", groupValidator)


            val classValidator: (String) -> Boolean = {
                val valid = (it.isBlank() || it.matches(Regex("^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)+\$")))
                if (!valid) System.out.println("Invalid main class")
                valid
            }
            val defaultMainClass = "${projectConfig[GROUP]}.${projectName.toLowerCase()}.${projectName.capitalize()}"
            projectConfig[MAIN_CLASS] = interaction
                .ask("Main Class (default: $defaultMainClass):", classValidator, defaultMainClass)


            projectConfig[VERSION] = interaction.ask("Version (default: 0.0.1):", "0.0.1")


            projectConfig[SHADOWJAR] = interaction.askYN("Use shadowjar in build script?", true)
            projectConfig[AUTODEPLOY] = interaction.askYN("Enable auto deploy to server after build?", true)


            projectConfig[BINTRAY] = interaction.askYN("Use bintray to distribute your package?", false)
            if (projectConfig[BINTRAY] == true) {
                projectConfig[BINTRAY_REPO] = interaction.askNotBlank("Bintray repo name:")
                projectConfig[VCS_URL] =
                    interaction.askNotBlank("VCS Url (example: https://gitlab.com/reactant/reactant.git):")
            }


            System.out.println("====================")
            projectConfig.forEach {
                System.out.println("${it.key}: ${it.value}")
            }
            System.out.println("====================")

            if (interaction.askYN("Is the project information correct? (Y/n): ", true)) {
                projectConfig[MAIN_CLASS_NAME] = projectConfig[MAIN_CLASS].toString().split(".").last()

                projectConfig[PACKAGE] = projectConfig[MAIN_CLASS].toString().split(".").dropLast(1).joinToString(".");
                return
            }
        }

    }

    private fun replacePlaceHolder() {
        progressBar.extraMessage = "Configuring"

        val freemarkerConfig: Configuration = Configuration(Configuration.VERSION_2_3_29);
        freemarkerConfig.setDirectoryForTemplateLoading(projectDir.absoluteFile);

        listOf(
            "build.gradle.kts",
            "settings.gradle.kts",
            "src/main/resources/plugin.yml",
            "src/main/kotlin/Plugin.kt"
        ).forEach {
            val template: Template = freemarkerConfig.getTemplate(it);
            val out = FileWriter("${projectDir.absolutePath}/$it");
            template.process(projectConfig.mapKeys { it.key.key }, out);
            out.close()
        }

        val packagePath = projectConfig[PACKAGE].toString().replace(".", "/");
        val mainClassName = projectConfig[MAIN_CLASS_NAME].toString()
        val mainClassPath = "${projectDir.absolutePath}/src/main/kotlin/$packagePath/$mainClassName.kt"
        val originClassPath = "${projectDir.absolutePath}/src/main/kotlin/Plugin.kt";
        File("${projectDir.absolutePath}/src/main/kotlin/$packagePath").mkdirs()
        File(originClassPath)
            .renameTo(File(mainClassPath))
            .let { success ->
                if (!success) {
                    logger.error("Failed move class file: $originClassPath to $mainClassPath");
                    throw IllegalStateException("Cannot move class to $mainClassPath")
                }
            }

        progressBar.step()
    }


    fun checkDirNotExist() {
        if (projectDir.exists()) {
            System.err.println("${projectDir.absolutePath} already exist");
            exitProcess(1);
        }
    }

    fun cloneRepository() {
        progressBar.extraMessage = "Clone"

        Git.cloneRepository()
            .setProgressMonitor(object : BatchingProgressMonitor() {
                override fun onUpdate(taskName: String?, workCurr: Int) {
                    progressBar.extraMessage = "Clone - $taskName"
                }

                override fun onUpdate(taskName: String?, workCurr: Int, workTotal: Int, percentDone: Int) {
                    progressBar.extraMessage = "Clone - $taskName $percentDone%"
                }

                override fun onEndTask(taskName: String?, workCurr: Int) {
                    progressBar.extraMessage = "Clone"
                }

                override fun onEndTask(taskName: String?, workCurr: Int, workTotal: Int, percentDone: Int) {
                    progressBar.extraMessage = "Clone"
                }

            })
            .setURI("https://gitlab.com/reactant/cli/blankplugin.git")
            .setBranchesToClone(listOf("refs/heads/$branch"))
            .setBranch("refs/heads/$branch")
            .setDirectory(projectDir)
            .call()
            .close()

        // Validate version
        val templateInfo = File(projectDir.absolutePath + "/template.json");
        if (!VersionUtils.validateVersion(templateInfo)) {
            progressBar.close()
            System.err.println(
                "CLI version is outdated, required version: ${VersionUtils.getTemplateVersion(templateInfo)}"
            )
            projectDir.deleteRecursively()
            exitProcess(1)
        }

        listOf(
            templateInfo,
            File(projectDir.absolutePath + "/.git")
        ).forEach { it.deleteRecursively() }

        progressBar.step();
    }
}
