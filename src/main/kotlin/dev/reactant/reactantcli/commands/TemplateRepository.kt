package dev.reactant.reactantcli.commands

import freemarker.template.Configuration
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.FileWriter
import java.net.URL

object TemplateRepository {
    const val baseUrl = "https://gitlab.com/reactant/cli/template/raw";

    fun getTemplateURL(path: String, branch: String = "master"): String = "$baseUrl/$branch/$path"

    fun getTemplateFile(path: String, branch: String = "master"): File {
        var folder = File(System.getProperty("java.io.tmpdir"));
        check(!(folder.exists() && folder.isFile));
        folder = File("${folder.absolutePath}/reactantcli/")
        if (!folder.exists()) folder.mkdirs()

        val tempPath = "${folder.absolutePath}/${DigestUtils.md5Hex(path)}";
        val tempFile = File(tempPath)
        tempFile.delete();
        FileUtils.copyURLToFile(
            URL(
                getTemplateURL(
                    path,
                    branch
                )
            ), tempFile
        );

        return tempFile;
    }

    fun writeTemplate(path: String, targetPath: String, dataModel: Any) {
        val templateFile = getTemplateFile(path);
        val freemarkerConfig: Configuration = Configuration(Configuration.VERSION_2_3_29);
        freemarkerConfig.setDirectoryForTemplateLoading(templateFile.parentFile);
        val template = freemarkerConfig.getTemplate(templateFile.name);
        if (File(targetPath).exists()) {
            println("[Failed] File already exist: $targetPath");
            return;
        }
        File(targetPath).parentFile.mkdirs()
        val out = FileWriter(targetPath);
        template.process(dataModel, out);
        out.close()
        println("[Success] Generated: $targetPath");
    }

}
