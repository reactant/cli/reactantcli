package dev.reactant.reactantcli.commands.generate

import dev.reactant.reactantcli.commands.generate.command.GenerateCommandClassCommand
import dev.reactant.reactantcli.commands.generate.component.GenerateComponentClassCommand
import picocli.CommandLine


@CommandLine.Command(
    name = "generate",
    aliases = ["gen"],
    description = ["Generate something with schema"],
    subcommands = [GenerateComponentClassCommand::class, GenerateCommandClassCommand::class]
)
class GenerateCommand : Runnable {
    override fun run() {
        CommandLine(this)
            .usage(System.out);
    }
}
