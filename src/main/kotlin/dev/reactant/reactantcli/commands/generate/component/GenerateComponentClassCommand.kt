package dev.reactant.reactantcli.commands.generate.component

import dev.reactant.reactantcli.commands.TemplateRepository
import org.slf4j.LoggerFactory
import picocli.CommandLine


@CommandLine.Command(
    name = "component",
    aliases = ["comp"],
    description = ["Generate a component class"]
)
class GenerateComponentClassCommand : Runnable {
    override fun run() {
        val dataModel = hashMapOf(
            "hook" to hook,
            "inspector" to inspector,
            "eventService" to eventService,
            "commandService" to commandService,
            "schedulerService" to schedulerService,
            "uiService" to uiService,
            "package" to classPath.split(".").let { if (it.size > 1) it.dropLast(1).joinToString(".") else "" },
            "className" to classPath.split(".").last()
        )
        TemplateRepository.writeTemplate(
            "type/component/Component.kt",
            "src/main/kotlin/${classPath.split(".").joinToString("/")}.kt",
            dataModel
        );
    }

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @CommandLine.Parameters(arity = "1", paramLabel = "FULL_CLASS_PATH")
    var classPath: String = ""

    @CommandLine.Option(
        names = ["-h", "--hook"],
        description = ["Implement LifeCycleHook in the component"]
    )
    var hook: Boolean = false;

    @CommandLine.Option(
        names = ["-i", "--inspector"],
        description = ["Implement LifeCycleInspector in the component"]
    )
    var inspector: Boolean = false;

    @CommandLine.Option(
        names = ["-e", "--event"],
        description = ["Inject EventService"]
    )
    var eventService: Boolean = false;

    @CommandLine.Option(
        names = ["-c", "--command"],
        description = ["Inject CommandService"]
    )
    var commandService: Boolean = false;

    @CommandLine.Option(
        names = ["-s", "--scheduler"],
        description = ["Inject SchedulerService"]
    )
    var schedulerService: Boolean = false;

    @CommandLine.Option(
        names = ["-u", "--ui"],
        description = ["Inject UIService"]
    )
    var uiService: Boolean = false;
}
