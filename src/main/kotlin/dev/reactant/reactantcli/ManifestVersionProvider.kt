package dev.reactant.reactantcli

import dev.reactant.reactantcli.utils.VersionUtils
import picocli.CommandLine

class ManifestVersionProvider : CommandLine.IVersionProvider {
    override fun getVersion(): Array<String> {
        val versionStr = VersionUtils.getCLIVersion()
        return arrayOf(
            "Reactant CLI v$versionStr"
        );
    }

}
